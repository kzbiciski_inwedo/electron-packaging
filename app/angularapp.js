
angular.module('app', ['angular-electron', 'SimpleAutoUpdater']);

angular.module('app').config(['remoteProvider', function(remoteProvider) {
    remoteProvider.register('robotjs');
    remoteProvider.register('./package.json');
}]);

angular.module('app').controller('myCtrl', ['robotjs', '$scope', 'http', 'fs', 'child_process', 'path', './package.json', 'auto-updater', 'Updater', '$rootScope', function(robotjs, $scope, http, fs, childProcess, path, packageJson, autoUpdater, Updater, $rootScope) {
    $scope.size = robotjs.getScreenSize();
    $scope.packageVersion = packageJson.version;
    $scope.updateServerAddress = 'localhost';

    // useful for debuging
    window.autoUpdater = autoUpdater;
    window.Updater = Updater;

    $rootScope.$on('checking-for-update', function(e) {
        console.log('checking-for-update', e);
    });
    $rootScope.$on('update-available', function(e) {
        console.log('update-available', e);
    });
    $rootScope.$on('update-not-available', function(e) {
        console.log('update-not-available', e);
    });
    $rootScope.$on('update-downloaded', function(e) {
        console.log('update-downloaded', e);
        if (window.confirm('Update is ready. Hit ok to update and restart the app.')) {
            Updater.doUpdate();
        }
    });

    $scope.runUpdateCheck = function() {
        Updater.init($scope.updateServerAddress);
        Updater.checkForUpdates();
    };

}]);
