(function() {
    'use strict';

    angular.module('SimpleAutoUpdater', [
        'angular-electron'
    ]);

    angular.module('SimpleAutoUpdater').config(['remoteProvider', function(remoteProvider) {
        remoteProvider.register('auto-updater');
    }]);

    angular
        .module('SimpleAutoUpdater')
        .service('Updater', Updater);

    Updater.$inject = ['path', 'child_process', 'auto-updater', '$rootScope'];
    function Updater(path, childProcess, autoUpdater, $rootScope) {
        var updateServerAddress = '';

        this.checkForUpdates = checkForUpdates;
        this.doUpdate = doUpdate;
        this.init = init;

        autoUpdater.on('checking-for-update', function(e) {
            $rootScope.$emit('checking-for-update', e);
        }).on('update-available', function(e) {
            $rootScope.$emit('update-available', e);
        }).on('update-not-available', function(e) {
            $rootScope.$emit('update-not-available', e);
        }).on('update-downloaded', function(e) {
            $rootScope.$emit('update-downloaded', e);
        });

        ////////////////

        function checkForUpdates() {
            var feedUrl = 'http://' + updateServerAddress;

            if (process.platform === 'win32') {
                feedUrl += '/installer';
            }
            else if (process.platform === 'darwin') {
                feedUrl += '/updateCheck.php';
            }
            else {
                console.warn('Auto updating is not supported in this OS.');
                return;
            }

            autoUpdater.setFeedURL(feedUrl);
            autoUpdater.checkForUpdates();
        }

        function doUpdate() {
            autoUpdater.quitAndInstall();
        }
        
        function init(serverAddress) {
            updateServerAddress = serverAddress;
        }
    }
})();
